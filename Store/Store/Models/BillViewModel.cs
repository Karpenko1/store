﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Store.Data;

namespace Store.Models
{
    public class BillViewModel : Bill
    {
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
        public IEnumerable<Customer> Customers { get; set; }
    }
}