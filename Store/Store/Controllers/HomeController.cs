﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Store.Data;
using Store.Models;

namespace Store.Controllers
{
    public class HomeController : Controller
    {
        StoreEntities db = new StoreEntities();
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult CreateBill()
        {
            BillViewModel model = new BillViewModel();
            model.Products = db.Product;
            model.Employees = db.Employee;
            model.Customers = db.Customer;
            return View(model);
        }


        [HttpPost]
        //public ActionResult CreateBill(List<BillProduct> list, int CustoerID, int EmployeeId, ...)
        public ActionResult CreateBill(Bill b, int[] IdProducts, int[] ProductsNumbers)
        {
            SortProductsIdAndNumbers(ref IdProducts, ref ProductsNumbers);
            CombineArraysByIdProducts(ref IdProducts, ref ProductsNumbers);
            
            decimal TotalPrice = 0;
            for (int i = 0; i < IdProducts.Length; i++)
            {
                TotalPrice += db.Product.Find(IdProducts[i]).Price * ProductsNumbers[i];          
            }
            b.TotalPrice = TotalPrice;
            b.PurchaseDate = DateTime.Now;

            //Bill TempBill = db.Bill.Last();
            //int BillId = TempBill.Id;

            //for (int i = 0; i < IdProductsDist.Length; i++)
            //{

            //}            
            for (int i = 0; i < IdProducts.Length; i++)
            {
                //BillProduct TempBP = new BillProduct();
                //TempBP.BillId = BillId;
                //TempBP.ProductId = ProductId;
                //db.BillProduct.Add(TempBP);
                b.BillProduct.Add(new BillProduct { ProductId = IdProducts[i], Number = ProductsNumbers[i] });
            }
            db.Bill.Add(b);
            db.SaveChanges();
            //db.Bill.Add(b);
            //db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult EditBill(int id)
        {
            Bill b = db.Bill.Find(id);
            BillViewModel model = new BillViewModel
            {
                Id = b.Id,
                PurchaseDate = b.PurchaseDate,
                TotalPrice = b.TotalPrice,
                EmployeeId = b.EmployeeId,
                CustomerId = b.CustomerId,
                Customer = b.Customer,
                Employee = b.Employee,
                BillProduct = b.BillProduct,
                Products = db.Product,
                Employees = db.Employee,
                Customers = db.Customer
            };
            return View(model);
        }

        
        [HttpPost]
        public ActionResult EditBill(Bill b, int[] IdProducts, int[] ProductsNumbers)
        {
            SortProductsIdAndNumbers(ref IdProducts, ref ProductsNumbers);
            CombineArraysByIdProducts(ref IdProducts, ref ProductsNumbers);

            decimal TotalPrice = 0;
            for (int i = 0; i < IdProducts.Length; i++)
            {
                TotalPrice += db.Product.Find(IdProducts[i]).Price * ProductsNumbers[i];
            }
            b.TotalPrice = TotalPrice;
            var editedBill = db.Bill.Find(b.Id);
            //editedBill.BillProduct.Clear();
            foreach (var bp in db.BillProduct)
            {
                if (b.Id == bp.BillId) db.BillProduct.Remove(bp);
            }
            for (int i = 0; i < IdProducts.Length; i++)
            {
                b.BillProduct.Add(new BillProduct { ProductId = IdProducts[i], Number = ProductsNumbers[i] });
            }
            editedBill.EmployeeId = b.EmployeeId;
            editedBill.CustomerId = b.CustomerId;
            editedBill.PurchaseDate = b.PurchaseDate;
            editedBill.TotalPrice = b.TotalPrice;
            editedBill.BillProduct = b.BillProduct;
            //db.Entry(b).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName("DeleteBill")]
        public ActionResult ConfirmDeleteBill(int id)
        {
            Bill bill = db.Bill.Find(id);
            return View(bill);
        }


        [HttpPost]
        public ActionResult DeleteBill(int id)
        {
            Bill bill = db.Bill.Find(id);
            foreach (var bp in db.BillProduct)
            {
                if (bill.Id == bp.BillId) db.BillProduct.Remove(bp);
            }
            db.Bill.Remove(bill);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        public ActionResult BillsTable()
        {
            List<Bill> BillsList = new List<Bill>();

            foreach (Bill b in db.Bill)
            {
                decimal Price = 0;
                foreach (BillProduct bp in b.BillProduct)
                {
                    Price += bp.Product.Price * bp.Number;
                }
                Bill Temp = new Bill
                {
                    Id = b.Id,
                    PurchaseDate = b.PurchaseDate,
                    CustomerId = b.CustomerId,
                    EmployeeId = b.EmployeeId,
                    Customer = b.Customer,
                    Employee = b.Employee,
                    //Customer = db.Customer.Find(b.CustomerId),
                    //Employee = db.Employee.Find(b.EmployeeId),
                    BillProduct = b.BillProduct,
                    TotalPrice = Price
                };
                BillsList.Add(Temp);
            }

            return View(BillsList);
        }


        public ActionResult Products()
        {
            return View();
        }


        [HttpGet]
        public ActionResult AddProduct()
        {
            return View();
        }


        [HttpPost]
        public ActionResult AddProduct(Product p)
        {
            db.Product.Add(p);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult EditProduct(int id)
        {
            var product = db.Product.Find(id);
            return View(product);
        }


        [HttpPost]
        public ActionResult EditProduct(Product p)
        {
            var temp_product = db.Product.Find(p.Id);
            temp_product.Name = p.Name;
            temp_product.Price = p.Price;
            temp_product.BillProduct = p.BillProduct;
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName("DeleteProduct")]
        public ActionResult ConfirmDeleteProduct(int id)
        {
            var product = db.Product.Find(id);
            return View(product);
        }


        [HttpPost]
        public ActionResult DeleteProduct(int Id)
        {
            var temp_product = db.Product.Find(Id);
            db.Product.Remove(temp_product);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ProductsTable()
        {
            List<Product> ProductsList = new List<Product>();

            foreach (Product p in db.Product)
            {
                Product Temp = new Product
                {
                    Id = p.Id,
                    Name = p.Name,
                    Price = p.Price,
                    BillProduct = p.BillProduct
                };
                ProductsList.Add(Temp);
            }

            return View(ProductsList);
        }


        public ActionResult Customers()
        {
            return View();
        }


        [HttpGet]
        public ActionResult AddCustomer()
        {
            return View();
        }


        [HttpPost]
        public ActionResult AddCustomer(Customer c)
        {
            db.Customer.Add(c);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult EditCustomer(int id)
        {
            var customer = db.Customer.Find(id);
            return View(customer);
        }


        [HttpPost]
        public ActionResult EditCustomer(Customer c)
        {
            var temp_customer = db.Customer.Find(c.Id);
            temp_customer.Name = c.Name;
            temp_customer.Email = c.Email;
            temp_customer.Bill = c.Bill;
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName("DeleteCustomer")]
        public ActionResult ConfirmDeleteCustomer(int id)
        {
            var customer = db.Customer.Find(id);
            return View(customer);
        }


        [HttpPost]
        public ActionResult DeleteCustomer(int Id)
        {
            var temp_customer = db.Customer.Find(Id);
            db.Customer.Remove(temp_customer);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult CustomersTable()
        {
            List<Customer> CustomersList = new List<Customer>();

            foreach (Customer c in db.Customer)
            {
                Customer Temp = new Customer
                {
                    Id = c.Id,
                    Name = c.Name,
                    Email = c.Email,
                    Bill = c.Bill
                };
                CustomersList.Add(Temp);
            }

            return View(CustomersList);
        }


        public ActionResult Employees()
        {
            return View();
        }


        [HttpGet]
        public ActionResult AddEmployee()
        {
            return View();
        }


        [HttpPost]
        public ActionResult AddEmployee(Employee e)
        {
            db.Employee.Add(e);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult EditEmployee(int id)
        {
            var employee = db.Employee.Find(id);
            return View(employee);
        }


        [HttpPost]
        public ActionResult EditEmployee(Employee e)
        {
            var temp_employee = db.Employee.Find(e.Id);
            temp_employee.Name = e.Name;
            temp_employee.Position = e.Position;
            temp_employee.Bill = e.Bill;
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName("DeleteEmployee")]
        public ActionResult ConfirmDeleteEmployee(int id)
        {
            var employee = db.Employee.Find(id);
            return View(employee);
        }


        [HttpPost]
        public ActionResult DeleteEmployee(int Id)
        {
            var temp_employee = db.Employee.Find(Id);
            db.Employee.Remove(temp_employee);
            db.SaveChanges();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult EmployeesTable()
        {
            List<Employee> EmployeesList = new List<Employee>();

            foreach (Employee e in db.Employee)
            {
                Employee Temp = new Employee
                {
                    Id = e.Id,
                    Name = e.Name,
                    Position = e.Position,
                    Bill = e.Bill
                };
                EmployeesList.Add(Temp);
            }

            return View(EmployeesList);
        }

        public void SortProductsIdAndNumbers(ref int[] IdProducts, ref int[] ProductsNumbers)
        {
            //if (IdProducts.Length != ProductsNumbers.Length) { }; //exception
            for (int i = 0; i < IdProducts.Length; i++)
            {
                for (int j = 0; j < IdProducts.Length - i - 1; j++)
                {
                    if (IdProducts[j] > IdProducts[j + 1])
                    {
                        int tempId = IdProducts[j];
                        IdProducts[j] = IdProducts[j + 1];
                        IdProducts[j + 1] = tempId;
                        int tempNumber = ProductsNumbers[j];
                        ProductsNumbers[j] = ProductsNumbers[j + 1];
                        ProductsNumbers[j + 1] = tempNumber;
                    }
                }
            }
        }

        public void CombineArraysByIdProducts(ref int[] IdProducts, ref int[] ProductsNumbers)
        {
            int excessLength = 0;
            int odds = 0;
            for (int i = 0; i < IdProducts.Length - 1; i++)
            {
                odds = 0;
                if (IdProducts[i] == IdProducts[i + 1])
                {
                    for (int j = i; (j < IdProducts.Length - 1) && (IdProducts[j] == IdProducts[j + 1]); j++)
                    {
                        ProductsNumbers[i] += ProductsNumbers[j + 1];
                        odds += 1;
                    }
                }
                if (excessLength != 0)
                {
                    IdProducts[i - excessLength] = IdProducts[i];
                    ProductsNumbers[i - excessLength] = ProductsNumbers[i];
                }
                i += odds;
                excessLength += odds;
            }

            bool IsLastElementOfArrayMissed = true;
            for (int i = 0; i < IdProducts.Length - excessLength; i++)
            {
                if (IdProducts[IdProducts.Length - 1] == IdProducts[i]) { IsLastElementOfArrayMissed = false; }                                
            }
            if (IsLastElementOfArrayMissed == true)
            {
                IdProducts[IdProducts.Length - excessLength - 1] = IdProducts[IdProducts.Length - 1];
                ProductsNumbers[IdProducts.Length - excessLength - 1] = ProductsNumbers[IdProducts.Length - 1];
            }

            Array.Resize(ref IdProducts, IdProducts.Length - excessLength);
            Array.Resize(ref ProductsNumbers, ProductsNumbers.Length - excessLength);
        }
    }
}